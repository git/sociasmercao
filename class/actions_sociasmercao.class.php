<?php
 /* Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
 *
 * This file is part of SociasMercao
 *
 * SociasMercao is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation; either version 3 of the 
 * License, or (at your option) any later version.
 *
 * SociasMercao is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    htdocs/sociasmercao/class/actions_sociasmercao.class.php
 * \ingroup sociasmercao
 * \brief   Example hook overload.
 *
 * what we want:
 *  - add a quota quantity definition for adherents
 *  - add a quota renovation type for adherents (monthly, by cash...)
 */

dol_include_once('/sociasmercao/class/modadherents.class.php', 'ModAdherents');
dol_include_once('/sociasmercao/class/quotamanagement.class.php', 'QuotaManagement');
require_once DOL_DOCUMENT_ROOT.'/core/class/html.form.class.php';

/**
 * Class ActionsSociasMercao
 */
class ActionsSociasMercao
{
    /**
    * @var DoliDB Database handler.
    */
    public $db;

    /**
    * @var string Error code (or message)
    */
    public $error = '';

    /**
    * @var array Errors
    */
    public $errors = array();


    /**
    * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
    */
    public $results = array();

    /**
    * @var string String displayed by executeHook() immediately after return
    */
    public $resprints;

    /**
    * Constructor
    *
    *  @param		DoliDB    $db    Database    handler
    */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
    * Execute action
    *
    * @param	array			$parameters		Array of parameters
    * @param	CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
    * @param	string			$action      	'add', 'update', 'view'
    * @return	int         					<0 if KO,
    *                           				=0 if OK but we want to process standard actions too,
    *                            				>0 if OK and we want to replace standard actions.
    */
    function getNomUrl($parameters,&$object,&$action)
    {
        global $db,$langs,$conf,$user;
        $this->resprints = '';
        return 0;
    }

    /**
    * Overloading the doActions function : replacing the parent's function with the one below
    *
    * @param   array           $parameters     Hook metadatas (context, etc...)
    * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
    * @param   string          $action         Current action (if set). Generally create or edit or null
    * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
    * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
    */
    public function doActions($parameters, &$object, &$action, $hookmanager)
    {
    	global $conf, $user, $langs;
    	
    	$error = 0; // Error counter

        /* TAKING REMOVING; UPDATING DATA FROM ADHERENTS FORM */
        // print_r($parameters); print_r($object); echo "action: " . $action;
        if (in_array($parameters['currentcontext'], array('membercard')))  
        {
            dol_syslog("DEBUG: Trigger for action '$action' launched. id=".$object->id);
            /* TODO: Dolibarr limitation, adherents module has no trigger when item is created, only when form is opened
            so we don't know if adherent is correctly created or not, neither it's id
            We are going to check is modadherents is creted when it's going to be edited, creating it then */
            if($action == 'edit') //member edition
            {
                $ToModify = new ModAdherents($this->db);
                $ToModify->ref = $object->id;
                $ToModify->quota = (GETPOST("quota", 'alpha')?trim(GETPOST("quota", 'alpha')):0);
            
                $ToModify->quota_type = (int)trim(GETPOST("QuotaType", 'alpha'));
                $ToModify->quota_freq = (int)trim(GETPOST("QuotaFreq", 'alpha'));
                $ret = $ToModify->myIsExistingObject(null, $ToModify->ref);
                if($ret > 0)
                {
                    dol_syslog("DEBUG: Trigger for action '$action' launched. id=".$object->id); 
                    $OldAdherent = new ModAdherents($this->db);
                    $OldAdherent->fetch(NULL,$ToModify->ref);
                    dol_syslog("DEBUG: action '$action' updating old modadherent id=".$OldAdherent->id);
                    //recovering original data
                    $ToModify->id = $OldAdherent->id;
                    $ToModify->label = $OldAdherent->label;
                    $ToModify->amount = $OldAdherent->amount;
                    $ToModify->fk_soc = $OldAdherent->fk_soc;
                    $ToModify->description = $OldAdherent->description;
                    $ToModify->note_public = $OldAdherent->note_public;
                    $ToModify->note_private = $OldAdherent->note_private;
                    $ToModify->date_creation = $OldAdherent->date_creation;
                    //no tms needed
                    $ToModify->fk_user_creat = $OldAdherent->fk_user_creat;
                    $ToModify->fk_user_modif = $OldAdherent->fk_user_modif;
                    $ToModify->import_key = $OldAdherent->import_key;
                    $ToModify->status = $OldAdherent->status;
                    $ToModify->credit = $OldAdherent->credit;
                    $ToModify->date_last_quota = $OldAdherent->date_last_quota;
                
                    if($OldAdherent->quota_freq != $ToModify->quota_freq || $OldAdherent->quota != $ToModify->quota)
                    {
                        $myQuotaManager = new QuotaManagement();
                        $myQuotaManager->upgradingQuota($ToModify);
                        if($myQuotaManager->date_last_quota)
                            $ToModify->date_last_quota = $myQuotaManager->date_last_quota;
                    }

                    $ToModify->update($user);
                } 
                else if($ret == 0)
                {
                    dol_syslog("DEBUG: action '$action' creating new modadherent");
                    $ToModify->credit = 0;
                    $myQuotaManager = new QuotaManagement();
                    $myQuotaManager->upgradingQuota($ToModify);
                    if($myQuotaManager->date_last_quota)
                        $ToModify->date_last_quota = $myQuotaManager->date_last_quota;
                    else
                    {
                        $now = new DateTime();
                        $ToModify->date_last_quota = $myQuotaManager->date_last_quota = $now->format('Y-m-d H:i:s');
                    }
                    $ToModify->create($user);
                }
            } 
            if($action == 'update') //member creation
            {   
                dol_syslog("DEBUG: Trigger for action '$action' launched. id=".$object->id); 	           	   
                $ToModify = new ModAdherents($this->db);
                $ToModify->ref = $object->id;
                $ToModify->quota = trim(GETPOST("quota", 'alpha'));
                if(!isset($ToModify->quota) || empty($ToModify->quota))
                    $ToModify->quota = 0;
                $ToModify->quota_type = (int)trim(GETPOST("QuotaType", 'alpha'));
                $ToModify->quota_freq = (int)trim(GETPOST("QuotaFreq", 'alpha'));
                $ret = $ToModify->myIsExistingObject(null, $ToModify->ref);
                if($ret > 0)
                {
                    $OldAdherent = new ModAdherents($this->db);
                    $OldAdherent->fetch(NULL,$ToModify->ref);
                    //recovering original data
                    $ToModify->id = $OldAdherent->id;
                    $ToModify->label = $OldAdherent->label;
                    $ToModify->amount = $OldAdherent->amount;
                    $ToModify->fk_soc = $OldAdherent->fk_soc;
                    $ToModify->description = $OldAdherent->description;
                    $ToModify->note_public = $OldAdherent->note_public;
                    $ToModify->note_private = $OldAdherent->note_private;
                    $ToModify->date_creation = $OldAdherent->date_creation;
                    //no tms needed
                    $ToModify->fk_user_creat = $OldAdherent->fk_user_creat;
                    $ToModify->fk_user_modif = $OldAdherent->fk_user_modif;
                    $ToModify->import_key = $OldAdherent->import_key;
                    $ToModify->status = $OldAdherent->status;
                    $ToModify->credit = $OldAdherent->credit;
                    $ToModify->date_last_quota = $OldAdherent->date_last_quota;
                   
                    if($OldAdherent->quota_freq != $ToModify->quota_freq || $OldAdherent->quota != $ToModify->quota)
                    {
                        $myQuotaManager = new QuotaManagement();
                        $myQuotaManager->upgradingQuota($ToModify);
                        if($myQuotaManager->date_last_quota)
                            $ToModify->date_last_quota = $myQuotaManager->date_last_quota;
                    }
                    $ToModify->update($user);
                } 
                else if( $ret == 0)
                {
                    dol_syslog("DEBUG: action '$action' creating new modadherent"); 
                    $ToModify->credit = 0;
                    $myQuotaManager = new QuotaManagement();
                    $myQuotaManager->upgradingQuota($ToModify);
                    if($myQuotaManager->date_last_quota)
                        $ToModify->date_last_quota = $myQuotaManager->date_last_quota;
                    else
                    {
                        $now = new DateTime();
                        $ToModify->date_last_quota = $myQuotaManager->date_last_quota = $now->format('Y-m-d H:i:s');
                    }
                    $ToModify->create($user);
                }        
            }
           //delete member
           elseif($action=='confirm_delete' && $user->rights->adherent->supprimer && $parameters['confirm'] == 'yes')
           {   
                $ToModify = new ModAdherents($this->db);
                $ToModify->fetch(NULL,$object->id);
                dol_syslog("DEBUG: action '$action' deleting old modadherent id=".$ToModify->id);
                $result = $ToModify->delete($user);
                //TODO: and what to do with it's quotas???
           }		
       }
       /* print_r($parameters); print_r($object); echo "action: " . $action; */
       if (in_array($parameters['currentcontext'], array('somecontext1','somecontext2')))	    // do something only for the context 'somecontext1' or 'somecontext2'
       {
           // Do what you want here...
           // You can for example call global vars like $fieldstosearchall to overwrite them, or update database depending on $action and $_POST values.
       }

       if (! $error) 
       {
           $this->results = array('myreturn' => 999);
           print $this->resprints;
           return 0; // or return 1 to replace standard code
       } 
       else 
       {
           $this->errors[] = 'Error message';
           return -1;
       }
    }

    /**
    * Overloading the doActions function : replacing the parent's function with the one below
    *
    * @param   array           $parameters     Hook metadatas (context, etc...)
    * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
    * @param   string          $action         Current action (if set). Generally create or edit or null
    * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
    * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
    */
    public function doMassActions($parameters, &$object, &$action, $hookmanager)
    {
        global $conf, $user, $langs;

        $error = 0; // Error counter

        /* print_r($parameters); print_r($object); echo "action: " . $action; */
        if (in_array($parameters['currentcontext'], array('somecontext1','somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
        {
            foreach($parameters['toselect'] as $objectid)
            {
                // Do action on each object id
            }
        }

        if (! $error)
        {
            $this->results = array('myreturn' => 999);
            $this->resprints = 'A text to show';
            return 0; // or return 1 to replace standard code
        } 
        else 
        {
            $this->errors[] = 'Error message';
            return -1;
        }
    }

    /**
    * Overloading the addMoreMassActions function : replacing the parent's function with the one below
    *
    * @param   array           $parameters     Hook metadatas (context, etc...)
    * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
    * @param   string          $action         Current action (if set). Generally create or edit or null
    * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
    * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
    */
    public function addMoreMassActions($parameters, &$object, &$action, $hookmanager)
    {
        global $conf, $user, $langs;

        $error = 0; // Error counter

        /* print_r($parameters); print_r($object); echo "action: " . $action; */
        if (in_array($parameters['currentcontext'], array('somecontext1','somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
        {
            $this->resprints = '<option value="0"'.($disabled?' disabled="disabled"':'').'>'.$langs->trans("MyModuleMassAction").'</option>';
        }

        if (! $error)
        {
            return 0; // or return 1 to replace standard code
        } 
        else 
        {
            $this->errors[] = 'Error message';
            return -1;
        }
    }

    /**
    * Execute action
    *
    * @param	array	$parameters		Array of parameters
    * @param   Object	$object		   	Object output on PDF
    * @param   string	$action     	'add', 'update', 'view'
    * @return  int 		        	<0 if KO,
    *                          		=0 if OK but we want to process standard actions too,
    *  	                            >0 if OK and we want to replace standard actions.
    */
    function beforePDFCreation($parameters, &$object, &$action)
    {
        global $conf, $user, $langs;
        global $hookmanager;

        $outputlangs=$langs;

        $ret=0; $deltemp=array();
        dol_syslog(get_class($this).'::executeHooks action='.$action);

        /* print_r($parameters); print_r($object); echo "action: " . $action; */
        if (in_array($parameters['currentcontext'], array('somecontext1','somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
        {  

        }
        return $ret;
    }

    /**
    * Execute action
    *
    * @param	array	$parameters		Array of parameters
    * @param   Object	$pdfhandler   	PDF builder handler
    * @param   string	$action     	'add', 'update', 'view'
    * @return  int 		        	<0 if KO,
    *                          		=0 if OK but we want to process standard actions too,
    *  	                            >0 if OK and we want to replace standard actions.
    */
    function afterPDFCreation($parameters, &$pdfhandler, &$action)
    {
        global $conf, $user, $langs;
        global $hookmanager;

        $outputlangs=$langs;

        $ret=0; $deltemp=array();
        dol_syslog(get_class($this).'::executeHooks action='.$action);

        /* print_r($parameters); print_r($object); echo "action: " . $action; */
        if (in_array($parameters['currentcontext'], array('somecontext1','somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
        {

        }
        return $ret;
    }

    /* Add here any other hooked methods... */
    /**
    * Overloading the formObjectOptions function : replacing the parent's function with the one below
    *
    * @param   array           $parameters     Hook metadatas (context, etc...)
    * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
    * @param   string          $action         Current action (if set). Generally create or edit or null
    * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
    * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
    */
    public function formObjectOptions($parameters, &$object, &$action, $hookmanager)
    {
        global $conf, $user, $langs;
        /**
        * Defining Quota Type
        */
        $QuotaType["manual"] = $langs->trans("PaymentManual");
        $QuotaType["monthly"] = $langs->trans("PaymentMonthly");

        $error = 0; // Error counter
		
        /* MODIFYING ADHERENTS FORM */
        /* print_r($parameters); print_r($object); echo "action: " . $action; */
        if (in_array($parameters['currentcontext'], array('membercard')))  
        {
        	$MyAdh = new ModAdherents($this->db);
            if($action == 'create') //member creation
            {
		        $this->resprints .= '<tr><td><p style="color:red">'.$langs->trans("UserCreationExplanation").'</p></td><td>';
		    }
            if($action == 'edit')
            {
             	$MyAdh->fetch(null,$object->id);
             	$form = new Form($this->db);

		        $this->resprints .= '<tr><td>'.$langs->trans("QuotaType").'</td><td>';
		        $this->resprints .= $form->selectarray("QuotaType", $MyAdh->array_quota_type, $id = $MyAdh->quota_type);
		        $this->resprints .= "</td></tr>";
     	        $this->resprints .= '<tr><td>'.$langs->trans("QuotaFreq").'</td><td>';
		        $this->resprints .= $form->selectarray("QuotaFreq", $MyAdh->array_quota_freq, $id = $MyAdh->quota_freq);
		        $this->resprints .= "</td></tr>";
                $this->resprints .= '<tr><td>'.$langs->trans("MemberQuota").'</td><td><input type="number" name="quota" size="20" value="'.(GETPOST('quota', 'alpha')?GETPOST('quota', 'alpha'):$MyAdh->quota).'" min="0"></td></tr>';
                $results['id'] = $object->id;        
             }
             if($action != 'edit' && $parameters['id'] > 0) //show member card
             {
                 $MyAdh->fetch(null,$object->id);
                 // we want to show the adherent credit here
                 $this->resprints .= '<tr><td><p style="font-weight:bold;">'.$langs->trans("Credit").'</p></td><td><input type="text" name="credit" size="20" value="'.$MyAdh->credit.'" style="font-weight: bold;" readonly></td></tr>';
             }
             else
             {
                $newModAdherent = new ModAdherents($this->db);
                $newModAdherent->ref = $object->id;
        
                $ret = $newModAdherent->myIsExistingObject(null, $object->id);
                if ($ret == 0)
                {
                    dol_syslog("DEBUG: -in formObjectOptions hook- action '$action' creating new modadherent");
                    $newModAdherent->credit = 0;
                    $myQuotaManager = new QuotaManagement();
                    $myQuotaManager->upgradingQuota($newModAdherent);
                    if($myQuotaManager->date_last_quota)
                        $newModAdherent->date_last_quota = $myQuotaManager->date_last_quota;
                    else
                    {
                        $now = new DateTime();
                        $newModAdherent->date_last_quota = $myQuotaManager->date_last_quota = $now->format('Y-m-d H:i:s');
                    }
                    $newModAdherent->create($user);
                    $this->resprints .= '<tr><td><p style="font-weight:bold;">'.$langs->trans("Credit").'</p></td><td><input type="text" name="credit" size="20" value="'.$newModAdherent->credit.'" style="font-weight: bold;" readonly></td></tr>';
                }
             }
        }
        /* print_r($parameters); print_r($object); echo "action: " . $action; */
        if (in_array($parameters['currentcontext'], array('somecontext1','somecontext2')))	    // do something only for the context 'somecontext1' or 'somecontext2'
        {
            // Do what you want here...
            // You can for example call global vars like $fieldstosearchall to overwrite them, or update database depending on $action and $_POST values.
        }

        if (! $error)
        {
            $this->results = array('myreturn' => 999);
            return 0; // or return 1 to replace standard code
        } 
        else 
        {
            $this->errors[] = 'Error message';
            return -1;
        }
    }
}
