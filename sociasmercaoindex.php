<?php
/* Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
 *
 * This file is part of SociasMercao
 *
 * SociasMercao is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation; either version 3 of the 
 * License, or (at your option) any later version.
 *
 * SociasMercao is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/sociasmercao/sociasmercaoindex.php
 *	\ingroup    sociasmercao
 *	\brief      Home page of sociasmercao top menu
 */

// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include substr($tmp, 0, ($i+1))."/main.inc.php";
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include dirname(substr($tmp, 0, ($i+1)))."/main.inc.php";
// Try main.inc.php using relative path
if (! $res && file_exists("../main.inc.php")) $res=@include "../main.inc.php";
if (! $res && file_exists("../../main.inc.php")) $res=@include "../../main.inc.php";
if (! $res && file_exists("../../../main.inc.php")) $res=@include "../../../main.inc.php";
if (! $res) die("Include of main fails");

require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';

// Load translation files required by the page
$langs->loadLangs(array("sociasmercao@sociasmercao"));

$action=GETPOST('action', 'alpha');


// Securite acces client
if (! $user->rights->sociasmercao->read) accessforbidden();


/*
 * Actions
 */

// None


/*
 * View
 */

llxHeader("",$langs->trans("SociasMercaoArea"));

print load_fiche_titre($langs->trans("SociasMercaoArea"),'','sociasmercao.png@sociasmercao');

print '<div class="fichecenter"><div class="fichethirdleft">';

print '<h2>' .$langs->trans("main_header1"). '</h2>';
print '<p> ' .$langs->trans("main_paragraph1"). '</p>';
print '<p> ' .$langs->trans("main_paragraph1b"). '</p>';

print '<h2>' .$langs->trans("main_header2"). '</h2>';
print '<p> ' .$langs->trans("main_paragraph2"). '</p>';
print '<a href="https://www.gnu.org/licenses/agpl-3.0.en.html" target="_blank">';
print ' <img src="./img/AGPLv3_Logo.png" style="width:110px" border-width:0;">';
print '</a>';


print '</div><div class="fichetwothirdright"><div class="ficheaddleft">';

print '</div></div></div>';

// End of page
llxFooter();
$db->close();
