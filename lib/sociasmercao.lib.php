<?php
/* Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
 *
 * This file is part of SociasMercao
 *
 * SociasMercao is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation; either version 3 of the 
 * License, or (at your option) any later version.
 *
 * SociasMercao is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    htdocs/sociasmercao/lib/sociasmercao.lib.php
 * \ingroup mymodule
 * \brief   Library files with common functions for SociasMercao
 */

/**
 * Prepare admin pages header
 *
 * @return array
 */
function sociasmercaoAdminPrepareHead()
{
	global $langs, $conf;

	$langs->load("sociasmercao@socaismercao");

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/sociasmercao/admin/setup.php", 1);
	$head[$h][1] = $langs->trans("Settings");
	$head[$h][2] = 'settings';
	$h++;
	$head[$h][0] = dol_buildpath("/sociasmercao/admin/about.php", 1);
	$head[$h][1] = $langs->trans("About");
	$head[$h][2] = 'about';
	$h++;

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//	'entity:+tabname:Title:@mymodule:/mymodule/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//	'entity:-tabname:Title:@mymodule:/mymodule/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, $object, $head, $h, 'sociasmercao');

	return $head;
}

function module_params()
{
	$arrayofparameters=array(
		'cronJobQuota'=>array('css'=>'minwidth200','enabled'=>1),
		'cronJobQuotaLastDate'=>array('css'=>'minwidth200','enabled'=>1, 'type'=>'date'),
	);

	return $arrayofparameters;
}
