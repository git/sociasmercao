-- Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
--
-- This file is part of SociasMercao
--
-- SociasMercao is free software; you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as 
-- published by the Free Software Foundation; either version 3 of the 
-- License, or (at your option) any later version.
--
-- SociasMercao is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.

CREATE TABLE llx_sociasmercao_modadherents(
        -- BEGIN MODULEBUILDER FIELDS
        rowid integer AUTO_INCREMENT PRIMARY KEY NOT NULL, 
        ref varchar(128) NOT NULL, 
        label varchar(255), 
        amount double(24,8) DEFAULT NULL, 
        fk_soc integer, 
        description text, 
        note_public text, 
        note_private text, 
        date_creation datetime NOT NULL, 
        tms timestamp NOT NULL, 
        fk_user_creat integer NOT NULL, 
        fk_user_modif integer, 
        import_key varchar(14), 
        status integer NOT NULL,
        -- END MODULEBUILDER FIELDS
	-- ADDED
	quota_type INTEGER DEFAULT 1 NOT NULL,
	quota_freq INTEGER DEFAULT 1 NOT NULL,
	quota REAL,
	credit REAL,
    	date_last_quota datetime
) ENGINE=innodb;
