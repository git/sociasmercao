-- Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
--
-- This file is part of SociasMercao
--
-- SociasMercao is free software; you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as 
-- published by the Free Software Foundation; either version 3 of the 
-- License, or (at your option) any later version.
--
-- SociasMercao is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.

-- BEGIN MODULEBUILDER INDEXES
ALTER TABLE llx_sociasmercao_movements ADD INDEX idx_sociasmercao_movements_rowid (rowid);
-- ALTER TABLE llx_sociasmercao_modadherents ADD INDEX idx_sociasmercao_modadherents_fk_soc (fk_soc);
-- ALTER TABLE llx_sociasmercao_modadherents ADD CONSTRAINT llx_sociasmercao_modadherents_fk_user_creat FOREIGN KEY (fk_user_creat) REFERENCES llx_user(rowid);
ALTER TABLE llx_sociasmercao_movements ADD INDEX idx_sociasmercao_movements_status (status);
-- we use the reference as unique reference to member rowid
ALTER TABLE llx_sociasmercao_movements ADD UNIQUE(rowid);
-- END MODULEBUILDER INDEXES

--ALTER TABLE llx_sociasmercao_modadherents ADD UNIQUE INDEX uk_sociasmercao_modadherents_fieldxy(fieldx, fieldy);
--ALTER TABLE llx_csociasmercao_modadherents ADD CONSTRAINT llx_sociasmercao_modadherents_fk_field FOREIGN KEY (fk_field) REFERENCES llx_sociasmercao_modadherents(rowid);
