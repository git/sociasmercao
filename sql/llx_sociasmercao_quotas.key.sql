-- Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
--
-- This file is part of SociasMercao
--
-- SociasMercao is free software; you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as 
-- published by the Free Software Foundation; either version 3 of the 
-- License, or (at your option) any later version.
--
-- SociasMercao is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.

-- BEGIN MODULEBUILDER INDEXES
ALTER TABLE llx_sociasmercao_quotas ADD INDEX idx_sociasmercao_quotas_rowid (rowid);
ALTER TABLE llx_sociasmercao_quotas ADD INDEX idx_sociasmercao_quotas_status (status);
-- we use the reference as unique reference to member rowid
ALTER TABLE llx_sociasmercao_quotas ADD UNIQUE(rowid);
-- unique combination of member ref and date_quota
ALTER TABLE llx_sociasmercao_quotas ADD UNIQUE(ref, date_quota);
-- END MODULEBUILDER INDEXES
