-- Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
--
-- This file is part of SociasMercao
--
-- SociasMercao is free software; you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as 
-- published by the Free Software Foundation; either version 3 of the 
-- License, or (at your option) any later version.
--
-- SociasMercao is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.

CREATE TABLE llx_sociasmercao_quotas_extrafields
(
  rowid                     integer AUTO_INCREMENT PRIMARY KEY,
  tms                       timestamp,
  fk_object                 integer NOT NULL,
  import_key                varchar(14)                                         -- import key
) ENGINE=innodb;





