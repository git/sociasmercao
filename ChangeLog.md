# CHANGELOG SOCIASMERCAO FOR <a href="https://www.dolibarr.org">DOLIBARR ERP CRM</a>

## 0.2 (2023/08/23)
+ FIX: index page logo size.
+ FIX: modadherent created always now when an adherent is, not using two steps.
+ FIX: permissions working now.
+ FIX: quota and credit can't be now nulls, default is 0.
+ FIX: some minor fixes.
+ IMPROVE: some code cleaning.

## 0.1 (2023/)
+ BUGFIX: movements are ordered by id of creation, not by Date.
+ IMPROVE: reordered movements list page, removed 'previous credit field'
+ IMPROVE: simplifying GUI, in new purchase page, the purchase kind is simply new purchase with no other options.
+ IMPROVE: new deposit by credit card option added, new deposit is now new deposit by cash now.
+ IMPROVE: reordered table field of movements in new purchase screen.
+ IMPROVE: in new purchase, in member movements table, the quantity of old purchases is shown as negative values.
+ minimum Dolibarr version needed set to 14.0

## 0.1.rc2 (2020/02/04)
+ BUGFIX: automatic quotas creation task
+ IMPROVE: custom filters in quotas and movements list added and improved
+ IMPROVE: automatic movement by default in new deposit and new purchase
+ IMPROVE: upgraded translations (es, en, fr)

## 0.1.rc1 (2019/10/25)
+ Initial public testing version.
+ adherents managment with quotas.
+ adherents money movement managment for the market shop.
+ etc.

```
 Copyright (C) 2019-2020 Pixelada S. Coop. And. <info(at)pixelada(dot)org)>
 Copyright (C) 2023 Joaquín Cuéllar <joa(dot)cuellar(at)riseup(dot)net)>
 
 This file is part of SociasMercao
 
 SociasMercao is free software; you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 
 SociasMercao is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.
```
