# Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
# Copyright (C) 2023 Joaquín Cuéllar <joa(dot)cuellar(at)riseup(dot)net>
#
# This file is part of SociasMercao
#
# SociasMercao is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation; either version 3 of the 
# License, or (at your option) any later version.
#
# SociasMercao is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.

#
# Generic
#

# Module label 'ModuleMyModuleName'
ModuleMyModuleName = SociasMercao
# Module description 'ModuleMyModuleDesc'
ModuleMyModuleDesc = SociasMercao es un módulo para la gestión de cuotas de socias y su uso en el pago de productos. 
SociasMercaoArea = ¡Bienvenida a Socias Mercao!
Load = Carga

# Module permissions
PerReadMovements = Consultar movimientos de socias con SociasMercao
PerCreateMovements = Crear nuevas compras o depósitos de socias con SociasMercao

#
# Admin page
#

MyModuleSetup = Configuración de Socias Mercao
Settings = Settings
MyModuleSetupPage = Página de configuración de Socias Mercao
cronJobQuota = Actualizar cuotas domiciliadas automáticamente
cronJobQuotaTooltip = Si es distinto de 0, cada mes se dará por domiciliada la cuota de una socia con esa opción de pago. Si es 0, este procedimiento no es automático.
cronJobQuotaLastDate = Fecha de la última actualización
cronJobQuotaLastDateTooltip = Registro de la última fecha en que se actualizaron las cuotas.

#
# Help page
#
SociasMercaoHelp = Ayuda con SociasMercao
main_help_header1 = Como obtener ayuda
main_help_paragraph1 = Sociasmercao es un módulo de Dolibarr desarrollado de forma comunitaria por participantes del Mercao social de Córdoba, La Tejedora. Siéntete libre de mejorarlo, pero por favor, comparte los cambios que veas.

#
# About page
#
About = Acerca de
MyModuleAbout = Acerca del módulo Socias Mercao
MyModuleAboutPage = Página acerca del módulo Socias Mercao

#
# Sample page
#
MyPageName = My page name

#
# Main page
#
main_header1 = Propósito de Socias Mercao!
main_paragraph1 = Socias Mercao es un módulo para dolibarr que extiende sus capacidades para poder manejar la gestión de socias de la asociacion La Tejedora.
main_paragraph1b = Está pensado para ser sencillo de usar, mantener y expandir. Si tienes dudas o ves errores, por favor escribe a la lista de correo pública: sociasmercao-support@nongnu.org , o abre un ticket de soporte en la página de su proyecto: <a href="https://savannah.nongnu.org/projects/sociasmercao/" target="_blank">https://savannah.nongnu.org/projects/sociasmercao/ </a>
main_header2 = Licenciado bajo la AGPLv3+!
main_paragraph2 = Como la aplicación está licenciada bajo la licencia libre AGPLv3+, estamos obligadas a enlazar donde descargar el código fuente de Socias Mercao, su repositorio de código está en: <a href="https://git.savannah.nongnu.org/git/sociasmercao.git" target="_blank"> https://git.savannah.nongnu.org/git/sociasmercao.git </a>

#
# Sample widget
#
MyWidget = My widget
MyWidgetDescription = My widget description

#status labels
Disabled = inhabilitada
NoDelay = al día
Delay = retraso
LongDelay = retraso largo
Paid = pagada
ParciallyPaid = parcialmente pagada
Unpaid = sin pagar

# TABS
##list adherents
Members = Miembros del Mercao
ListMembers = Listado de miembros
NewMember = Nuevo miembro
DateLastQuota = Fecha última cuota
Movements = Movimientos
ListMovements = Listado de movimientos
NewPaymentFromCredit = Nueva compra en tienda
NewDepositToCredit = Nuevo depósito en cuenta
CreditPrev = Crédito previo
CreditFinal = Crédito final
##quotaManagement
QuotaManagement = Gestión de cuotas
ListQuotas = Listado de cuotas
QuotaToPay = Cuota a pagar
DateQuota = Fecha de cuota
Quotas = Cuotas

# new fields, in members forms

QuotaType = forma de pago de cuota
QuotaFreq = frecuencia de pago de cuota
MemberQuota = cuota
PaymentByCash = en efectivo
PaymentByBank = por banco
monthly = mensual
3monthly = trimestral
6monthly = semestral
yearly = anual
UserCreationExplanation = Para los datos de pago de cuota, cree primero la socia y luego editela
Credit = saldo actual

# lists
selectMemberForNewPayment = Selecciona una socia para una nueva COMPRA
selectMemberForNewDeposit = Selecciona una socia para un nuevo DEPÓSITO
link = enlace
buy = a comprar!
deposit = a depositar!

# fields, in new movement forms
MovementType = Tipo de movimiento
NewBuy = Nueva compra
OtherPayments = Otros pagos
NewDeposit = Nuevo depósito en efectivo
ProductBack = Devolución productos
BottlesBack = Devolución envases
NewDepositFromCredit = Nuevo depósito con tarjeta
Member = Socia

# custom errors
ErrorFieldValuePositive = Debe de tener valor positivo
