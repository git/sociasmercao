# Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
# Copyright (C) 2023 Joaquín Cuéllar <joa(dot)cuellar(at)riseup(dot)net>
#
# This file is part of SociasMercao
#
# SociasMercao is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation; either version 3 of the 
# License, or (at your option) any later version.
#
# SociasMercao is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.

#
# Generic
#

# Module label 'ModuleMyModuleName'
ModuleMyModuleName = SociasMercao
# Module description 'ModuleMyModuleDesc'
ModuleMyModuleDesc = SociasMercao is a Dolibarr's module for advanced adherents management, adding them a quota that can be used to buy products. 
SociasMercaoArea = ¡Welcome to SociasMercao!

# Module permissions
PerReadMovements = Read movements of SociasMercao
PerCreateMovements = Create/Update movements of SociasMercao


#
# Admin page
#
MyModuleSetup = configuración de SociasMercao
Settings = Settings
MyModuleSetupPage = MyModule setup page
MYMODULE_MYPARAM1 = My param 1
MYMODULE_MYPARAM1Tooltip = My param 1 tooltip
MYMODULE_MYPARAM2=My param 2
MYMODULE_MYPARAM2Tooltip=My param 2 tooltip

#
# Help page
#
SociasMercaoHelp = Help with SociasMercao
main_help_header1 = How to obtain help
main_help_paragraph1 = Sociasmercao is a communitary dolibarr's module supported and developed by people from the Social Market of Córdoba -La Tejedora-, feel free to improve it, and please, share back your changes.

#
# About page
#
About = About
MyModuleAbout = About MyModule
MyModuleAboutPage = MyModule about page

#
# Sample page
#
MyPageName = My page name

#
# Main page
#
main_header1 = Socias Mercao Purpose!
main_paragraph1 = Socias Mercao is a dolibarr's module intended to upgrade Dolibarr's options, principally for accomplishing advanced members management used at the La Tejedora Association (https://latejedora.org).
main_paragraph1b = Is designed to be simple and easy to be used, upgraded, fixed and improved... If you have any question or you have seen any error, please! don't hesitate to write in the public mailing list: sociasmercao-support@nongnu.org , or open a support ticket at: <a href="https://savannah.nongnu.org/projects/sociasmercao/" target="_blank">https://savannah.nongnu.org/projects/sociasmercao/ </a>
main_header2 = Licensed under the AGPLv3+!
main_paragraph2 = As Socias Mercao is licensed under the AGPLv3+ free software license, we have to link where actual source code can be downloaded, it's updated source code repository is hosted at: <a href="https://git.savannah.nongnu.org/git/sociasmercao.git" target="_blank">https://git.savannah.nongnu.org/git/sociasmercao.git </a>

#
# Sample widget
#
MyWidget = My widget
MyWidgetDescription = My widget description

# TABS
##list adherents
ModAdherents = Mercao Members


# fields

QuotaType = Fee payment method
MemberQuota = Fee amount
PaymentManual = Manual payment
PaymentMonthly = Monthly payment

# fields, in new movement forms
MovementType = Movement type
NewBuy = New buy
OtherPayments = Other payments
NewDeposit = New deposit with cash
ProductBack = Product back
BottlesBack = Bottle back
NewDepositFromCredit = New deposit with credit card
Member = Member
