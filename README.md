# Socias Mercao module FOR <a href="https://www.dolibarr.org">DOLIBARR ERP CRM</a>

## Features

This module expand adherents module, what we want:

* members paying quotas (manually, monthly, etc.)
* member's credit usable for association shop payments. (credit can be negative)
* automated tasks for managing quotas:
    - quota status
    - automatic quota refresh
    - etc.


### Translations

Translations can be define manually by editing files into directories *langs*.
Module is developed with the spanish translation in mind, but we are opened to incorporate french and english options if needed.

<!--
This module contains also a sample configuration for Transifex, under the hidden directory [.tx](.tx), so it is possible to manage translation using this service. 

For more informations, see the [translator's documentation](https://wiki.dolibarr.org/index.php/Translator_documentation).

There is a [Transifex project](https://transifex.com/projects/p/dolibarr-module-template) for this module.
-->


<!--

Install
-------

### From the ZIP file and GUI interface

- If you get the module in a zip file (like when downloading it from the market place [Dolistore](https://www.dolistore.com)), go into
menu ```Home - Setup - Modules - Deploy external module``` and upload the zip file.


Note: If this screen tell you there is no custom directory, check your setup is correct: 

- In your Dolibarr installation directory, edit the ```htdocs/conf/conf.php``` file and check that following lines are not commented:

    ```php
    //$dolibarr_main_url_root_alt ...
    //$dolibarr_main_document_root_alt ...
    ```

- Uncomment them if necessary (delete the leading ```//```) and assign a sensible value according to your Dolibarr installation

    For example :

    - GNU/Linux:
        ```php
        $dolibarr_main_url_root_alt = '/custom';
        $dolibarr_main_document_root_alt = '/var/www/Dolibarr/htdocs/custom';
        ```       
### From a GIT repository

- Clone the repository in ```$dolibarr_main_document_root_alt/mymodule```

```sh
cd ....../custom
git clone git@github.com:gitlogin/mymodule.git mymodule
```

### <a name="final_steps"></a>Final steps

From your browser:

  - Log into Dolibarr as a super-administrator
  - Go to "Setup" -> "Modules"
  - You should now be able to find and enable the module


Copyright
-----------------

Copyright (C) 2019, the developers:

+  Pixelada S. Coop. And. <info(at)pixelada(dot)org>

License
--------

 SociasMercao is free software; you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as 
 published by the Free Software Foundation; either version 3 of the 
 License, or (at your option) any later version.
 
 SociasMercao is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.

 
![AGPLv3 logo](img/AGPLv3_Logo.png)

See file COPYING for more information.

