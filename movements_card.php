<?php
/* Copyright (C) 2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2019 Pixelada S. Coop. And. <info(at)pixelada(dot)org>
 *
 * This file is part of SociasMercao
 *
 * SociasMercao is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation; either version 3 of the 
 * License, or (at your option) any later version.
 *
 * SociasMercao is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with SociasMercao. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *   	\file       htdocs/socaismercado/movements_card.php
 *		\ingroup    sociasmercao
 *		\brief      Page to create and view movements
 */

//if (! defined('NOREQUIREDB'))              define('NOREQUIREDB','1');					// Do not create database handler $db
//if (! defined('NOREQUIREUSER'))            define('NOREQUIREUSER','1');				// Do not load object $user
//if (! defined('NOREQUIRESOC'))             define('NOREQUIRESOC','1');				// Do not load object $mysoc
//if (! defined('NOREQUIRETRAN'))            define('NOREQUIRETRAN','1');				// Do not load object $langs
//if (! defined('NOSCANGETFORINJECTION'))    define('NOSCANGETFORINJECTION','1');		// Do not check injection attack on GET parameters
//if (! defined('NOSCANPOSTFORINJECTION'))   define('NOSCANPOSTFORINJECTION','1');		// Do not check injection attack on POST parameters
//if (! defined('NOCSRFCHECK'))              define('NOCSRFCHECK','1');					// Do not check CSRF attack (test on referer + on token if option MAIN_SECURITY_CSRF_WITH_TOKEN is on).
//if (! defined('NOTOKENRENEWAL'))           define('NOTOKENRENEWAL','1');				// Do not roll the Anti CSRF token (used if MAIN_SECURITY_CSRF_WITH_TOKEN is on)
//if (! defined('NOSTYLECHECK'))             define('NOSTYLECHECK','1');				// Do not check style html tag into posted data
//if (! defined('NOREQUIREMENU'))            define('NOREQUIREMENU','1');				// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))            define('NOREQUIREHTML','1');				// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))            define('NOREQUIREAJAX','1');       	  	// Do not load ajax.lib.php library
//if (! defined("NOLOGIN"))                  define("NOLOGIN",'1');						// If this page is public (can be called outside logged session). This include the NOIPCHECK too.
//if (! defined('NOIPCHECK'))                define('NOIPCHECK','1');					// Do not check IP defined into conf $dolibarr_main_restrict_ip
//if (! defined("MAIN_LANG_DEFAULT"))        define('MAIN_LANG_DEFAULT','auto');					// Force lang to a particular value
//if (! defined("MAIN_AUTHENTICATION_MODE")) define('MAIN_AUTHENTICATION_MODE','aloginmodule');		// Force authentication handler
//if (! defined("NOREDIRECTBYMAINTOLOGIN"))  define('NOREDIRECTBYMAINTOLOGIN',1);		// The main.inc.php does not make a redirect if not logged, instead show simple error message
//if (! defined("FORCECSP"))                 define('FORCECSP','none');					// Disable all Content Security Policies


// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include substr($tmp, 0, ($i+1))."/main.inc.php";
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include dirname(substr($tmp, 0, ($i+1)))."/main.inc.php";
// Try main.inc.php using relative path
if (! $res && file_exists("../main.inc.php")) $res=@include "../main.inc.php";
if (! $res && file_exists("../../main.inc.php")) $res=@include "../../main.inc.php";
if (! $res && file_exists("../../../main.inc.php")) $res=@include "../../../main.inc.php";
if (! $res) die("Include of main fails");

require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent.class.php';

// load mymodule libraries
require_once __DIR__ . '/class/modadherents.class.php';
require_once __DIR__ . '/class/movements.class.php';
require_once __DIR__ . '/class/quotamanagement.class.php';

// Load translation files required by the page
$langs->loadLangs(array("sociasmercao@sociasmercao","other"));

if (! $user->rights->sociasmercao->edit) accessforbidden();

// Get parameters
$id			= GETPOST('id', 'int');
$ref        = GETPOST('ref', 'alpha');
$action		= GETPOST('action', 'aZ09');
$confirm    = GETPOST('confirm', 'alpha');
$cancel     = GETPOST('cancel', 'aZ09');
$contextpage= GETPOST('contextpage','aZ')?GETPOST('contextpage','aZ'):'modadherentscard';   // To manage different context of search
$backtopage = GETPOST('backtopage', 'alpha');

// Initialize technical objects
$object=new Movements($db);
$extrafields = new ExtraFields($db);
$diroutputmassaction=$conf->sociasmercao->dir_output . '/temp/massgeneration/'.$user->id;
$hookmanager->initHooks(array('modadherentscard','globalcard'));     // Note that conf->hooks_modules contains array
// Fetch optionals attributes and labels
$extralabels = $extrafields->fetch_name_optionals_label($object->table_element);
$search_array_options=$extrafields->getOptionalsFromPost($object->table_element,'','search_');

// Initialize array of search criterias
$search_all=trim(GETPOST("search_all",'alpha'));
$search=array();
foreach($object->fields as $key => $val)
{
	if (GETPOST('search_'.$key,'alpha')) $search[$key]=GETPOST('search_'.$key,'alpha');
}

if (empty($action) && empty($id) && empty($ref)) $action='view';

// Load object
//include DOL_DOCUMENT_ROOT.'/core/actions_fetchobject.inc.php';  // Must be include, not include_once  // Must be include, not include_once. Include fetch and fetch_thirdparty but not fetch_optionals

// Security check - Protection if external user
//if ($user->societe_id > 0) access_forbidden();
//if ($user->societe_id > 0) $socid = $user->societe_id;
//$isdraft = (($object->statut == ModAdherents::STATUS_DRAFT) ? 1 : 0);
//$result = restrictedArea($user, 'sociasmercao', $object->id, '', '', 'fk_soc', 'rowid', $isdraft);

$object->fields = dol_sort_array($object->fields, 'position');
$arrayfields = dol_sort_array($arrayfields, 'position');

/*
 * Actions
 *
 * Put here all code to do according to value of "action" parameter
 */

$parameters=array();
$reshook=$hookmanager->executeHooks('doActions',$parameters,$object,$action);    // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

if (empty($reshook))
{
	$error=0;

	$permissiontoadd = $user->rights->sociasmercao->edit;
	$permissiontodelete = $user->rights->sociasmercao->edit || ($permissiontoadd && $object->status == 0);
   //$backurlforlist = dol_buildpath('/sociasmercao/movements_list.php',1);
	//if (empty($backtopage)) {
	    //if (empty($id)) $backtopage = $backurlforlist;
	    //else 
	// $backtopage = dol_buildpath('/sociasmercao/movements_card.php',1).(($action == 'newPayment' || $action == 'newDeposit')?'?action='.$action:'?action=view').($id > 0 ? '&id='.$id : '__ID__');
	//$backtopage = dol_buildpath('/sociasmercao/movements_card.php',1).($id > 0 ? '?id='.$id : '__ID__');
   // 	}
	$triggermodname = 'SOCIASMERCAO_MODADHERENTS_MODIFY';	// Name of trigger action code to execute when we modify record

	// Actions cancel, add, update, delete or clone
	//include DOL_DOCUMENT_ROOT.'/core/actions_addupdatedelete.inc.php';
	// Action to add record
   if(($action == 'addDeposit' || $action == 'addPayment') && ! empty($permissiontoadd))
   {
       $error = 0;
	   foreach ($object->fields as $key => $val)
	   {
		   if (in_array($key, array('rowid', 'entity', 'tms', 'fk_user_creat', 'fk_user_modif', 'import_key'))) continue;	// Ignore special fields
		
		   // Set value to insert
		   if (in_array($object->fields[$key]['type'], array('text', 'html'))) 
		   {
			   $value = GETPOST($key, 'none');
		   } 
		   elseif ($object->fields[$key]['type']=='date') 
		   {
			   $value = dol_mktime((GETPOST($key.'hour')?GETPOST($key.'hour'):date('H')), (GETPOST($key.'min')?GETPOST($key.'min'):date('i')), 0, GETPOST($key.'month'), GETPOST($key.'day'), GETPOST($key.'year'));
		   } 
		   elseif ($object->fields[$key]['type']=='datetime') 
		   {
			   $value = dol_mktime((GETPOST($key.'hour')?GETPOST($key.'hour'):date('H')), (GETPOST($key.'min')?GETPOST($key.'min'):date('i')), 0, GETPOST($key.'month'), GETPOST($key.'day'), GETPOST($key.'year'));
		   }
		   elseif ($object->fields[$key]['type']=='price') 
		   {
			   $value = price2num(GETPOST($key));
		   } 
		   else 
		   {
			   $value = GETPOST($key, 'alpha');
		   }
		   if (preg_match('/^integer:/i', $object->fields[$key]['type']) && $value == '-1') $value='';		// This is an implicit foreign key field
		   if (! empty($object->fields[$key]['foreignkey']) && $value == '-1') $value='';					// This is an explicit foreign key field
		   //print "value with key: ".$key." , value:".$value."<br/>";
		   // default values
		   if (in_array($key, array('status')))
		   	$value = 1;
		   $object->$key=$value;
		   
		   if ($val['notnull'] > 0 && $object->$key == '' && ! is_null($val['default']) && $val['default'] == '(PROV)')
		   {
		      $object->$key = '(PROV)';
		   }
		   if ($val['notnull'] > 0 && $object->$key == '' && is_null($val['default']))
		   {
			   $error++;
			   setEventMessages($langs->trans("ErrorFieldRequired", $langs->transnoentitiesnoconv($val['label'])), null, 'errors');
		   }
	   	
		   if($key == 'amount')
		   {
		      if($object->amount <=0)
			   {
			      $error++;
			      setEventMessages($langs->trans("ErrorFieldValuePositive", $langs->transnoentitiesnoconv($val['label'])), null, 'errors');                  				   
			   }			   
		   }
		
	   }

	   if (! $error)
	   {
           $myModAdherents = new ModAdherents($db);
           $myModAdherents->fetch(null,$object->ref);  
           //updating quotas and credits
           $object->credit_prev = $myModAdherents->credit;
           if($object->amount <= 0) $object->amount *= -1;
           if($action == 'addPayment') $object->amount *= -1;
           $myModAdherents->credit += $object->amount;        
           $object->credit_final = $myModAdherents->credit;
           $object->quota_type = $myModAdherents->quota_type;
           if($action == 'addPayment') $object->mov_type = 20;	   	
		   $result = $object->create($user);
		   if ($result > 0)
		   {
               // Creation OK
               //TODO
               //in new deposit
               //take, adherent, user, and amount, and fill pending quota or quotas
               //afterwards we can upgrade modadherent status
               if($action == 'addDeposit' && $object->mov_type == 10) //if new quota deposit
               {
                   dol_syslog('in movements_card.php, calling QuotaManagement->PayQuota()', LOG_DEBUG);
                   $my_quota_management_system = new QuotaManagement($db);
                   $my_quota_management_system->payQuota($myModAdherents, $object->amount, $object->date_creation);
               }
               $myModAdherents->update($user);
			   $urltogo = $backtopage = dol_buildpath('/sociasmercao/movements_list.php?id=' . $object->ref, 1);
			   header("Location: ".$urltogo);
			   exit;		
		   }
		   else
		   {
			   // Creation KO
			   if (! empty($object->errors)) setEventMessages(null, $object->errors, 'errors');
			   else  setEventMessages($object->error, null, 'errors');
			   if($action == 'addPayment') $action='newPayment';
			   else $action='newDeposit';
			   $id = $object->ref;
		   }
	   }
	   else
	   {//TODO
		   if($action == 'addPayment') $action='newPayment';
			else $action='newDeposit';
		   $id = $object->ref;
	   } 
    }
	

	// Actions when linking object each other
	include DOL_DOCUMENT_ROOT.'/core/actions_dellink.inc.php';		// Must be include, not include_once

	// Actions when printing a doc from card
	include DOL_DOCUMENT_ROOT.'/core/actions_printing.inc.php';

	// Actions to send emails
	$trigger_name='MODADHERENTS_SENTBYMAIL';
	$autocopy='MAIN_MAIL_AUTOCOPY_MODADHERENTS_TO';
	$trackid='modadherents'.$object->id;
	include DOL_DOCUMENT_ROOT.'/core/actions_sendmails.inc.php';
}

/*
 * View
 *
 * Put here all code to build page
 */
$form=new Form($db);
$formfile=new FormFile($db);

//we repeat
if($id)
{
	//adherent attributes
	$myAdherent = new adherent($db);
	$myAdherent->fetch($id,null);
	$myModAdherents = new ModAdherents($db);
	$myModAdherents->fetch(null,$id);  
}

llxHeader('','Movements','');

// Example : Adding jquery code
print '<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	function init_myfunc()
	{
		jQuery("#myid").removeAttr(\'disabled\');
		jQuery("#myid").attr(\'disabled\',\'disabled\');
	}
	init_myfunc();
	jQuery("#mybutton").click(function() {
		init_myfunc();
	});
});
</script>';
//VIEW
// Part to create, new_payment
if (($action == 'newPayment' || $action == 'newDeposit') && $id > 0)
{
	if($action == 'newPayment') print load_fiche_titre($langs->trans("NewPaymentFromCredit"));
	else print load_fiche_titre($langs->trans(NewDepositToCredit));
	
	$object->ref = $id; //memberId
	
	print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	if($action == 'newPayment') print '<input type="hidden" name="action" value="addPayment">';
	else print '<input type="hidden" name="action" value="addDeposit">';
	print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';

	dol_fiche_head(array(), '');
	print '<table class="border centpercent">'."\n";

	print '<tr><td>ID</td><td>'.$id.'</td></tr>';
	print '<tr><td>'.$langs->trans('member').'</td><td>'.$myAdherent->lastname.', '.$myAdherent->firstname.'</td></tr>';
	print '<tr><td>'.$langs->trans('Credit').'</td><td><bold>'.$myModAdherents->credit.'</bold></td></tr>';
    print '<tr><td>'.$langs->trans('Status').'</td><td>'.$myModAdherents->showOutputField($myModAdherents->fields['status'], 'status', $myModAdherents->credit).'</td></tr>';
	print '</table>'."\n";
	//last 5 movements
	//$myMovement = new Movements($db);
	$records = $object->fetchLastRecords($id);
    // Definition of fields for list
    $arrayfields=array();
    foreach($object->fields as $key => $val)
    {
        // If $val['visible']==0, then we never show the field
        if (! empty($val['visible'])) $arrayfields['t.'.$key]=array('label'=>$val['label'], 'checked'=>(($val['visible']<0)?0:1), 'enabled'=>$val['enabled'], 'position'=>$val['position']);
    }
 	//insert table of last 5 movements of member
    print '<div class="div-table-responsive">';		// You can use div-table-responsive-no-min if you dont need reserved height for your table
    print '<table class="tagtable liste">';

    $param = '';
    // Fields title label
    // --------------------------------------------------------------------
    print '<tr class="liste_titre">'; 
    foreach($object->fields as $key => $val)
    {
        $cssforfield='';
        if (in_array($val['type'], array('date','datetime','timestamp'))) $cssforfield.=($cssforfield?' ':'').'center';
        if (in_array($val['type'], array('timestamp'))) $cssforfield.=($cssforfield?' ':'').'nowrap';
        if ($key == 'status') $cssforfield.=($cssforfield?' ':'').'center';
        if (!empty($arrayfields['t.'.$key]['checked']))
        {
            print getTitleFieldOfList($object->fields[$key]['label'], 0, $_SERVER['PHP_SELF'], $key, '', $param, '', '', '', '')."\n";
        }	
    }
    // Extra fields
    include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_list_search_title.tpl.php';
    // Hook fields
    $parameters=array('arrayfields'=>$arrayfields,'param'=>$param,'sortfield'=>$sortfield,'sortorder'=>$sortorder);
    $reshook=$hookmanager->executeHooks('printFieldListTitle', $parameters, $object);    // Note that $action and $object may have been modified by hook
    print $hookmanager->resPrint;
    //print getTitleFieldOfList($selectedfields, 0, $_SERVER["PHP_SELF"],'','','','align="center"',$sortfield,$sortorder,'maxwidthsearch ')."\n";
    print '</tr>'."\n";
    // Detect if we need a fetch on each output line
    $needToFetchEachLine=0;
    if (is_array($extrafields->attributes[$object->table_element]['computed']) && count($extrafields->attributes[$object->table_element]['computed']) > 0)
    {
        foreach ($extrafields->attributes[$object->table_element]['computed'] as $key => $val)
        {
            if (preg_match('/\$object/',$val)) $needToFetchEachLine++;  // There is at least one compute field that use $object
        }
    }

    // Loop on() record
    // --------------------------------------------------------------------
    $totalarray=array();
    if($records !== -1)
    {
        foreach($records as $obj)
        {
            if (empty($obj)) break;		// Should not happen
	
            // Show here line of result
            print '<tr class="oddeven">';         
            foreach($obj->fields as $key => $val)
            {
                $cssforfield='';
                if (in_array($val['type'], array('date','datetime','timestamp'))) $cssforfield.=($cssforfield?' ':'').'center';
                elseif ($key == 'status') $cssforfield.=($cssforfield?' ':'').'center';
                if (in_array($val['type'], array('timestamp'))) $cssforfield.=($cssforfield?' ':'').'nowrap';
                elseif ($key == 'ref') $cssforfield.=($cssforfield?' ':'').'nowrap';
                if (! empty($arrayfields['t.'.$key]['checked']))
                {
                    print '<td';
                    if ($cssforfield || $val['css']) print ' class="';
                    print $cssforfield;
                    if ($cssforfield && $val['css']) print ' ';
                    print $val['css'];
                    if ($cssforfield || $val['css']) print '"';
                    print '>';

                    if (in_array($val['type'], array('date','datetime','timestamp'))) 
                        print $obj->showOutputField($val, $key, $db->jdate($obj->$key), '');
                    else 
                        print $obj->showOutputField($val, $key, $obj->$key);
                    
                    
                    print '</td>';
                    if (! $i) $totalarray['nbfield']++;
			      
                    if (! empty($val['isameasure'])) 
                    {
                        if (! $i) $totalarray['pos'][$totalarray['nbfield']]=$key;
                        $totalarray['val'][$key] += $obj->$key;
                    }
                }
            }
            // Extra fields
            include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_list_print_fields.tpl.php';
            // Fields from hook
            $parameters=array('arrayfields'=>$arrayfields, 'obj'=>$object);
            $reshook=$hookmanager->executeHooks('printFieldListValue', $parameters, $object);    // Note that $action and $object may have been modified by hook
            print $hookmanager->resPrint;	      
            print '</td>';
            print '</tr>';
        }
    }
    else
    {
        $colspan=1;
        foreach($arrayfields as $key => $val) { if (! empty($val['checked'])) $colspan++; }
        print '<tr><td colspan="'.$colspan.'" class="opacitymedium">'.$langs->trans("NoRecordFound").'</td></tr>';
    }
   
    $parameters=array('arrayfields'=>$arrayfields, 'sql'=>$sql);
    $reshook=$hookmanager->executeHooks('printFieldListFooter', $parameters, $object);    // Note that $action and $object may have been modified by hook
    print $hookmanager->resPrint;

    print '</table>'."\n";
    print '</div>'."\n";
    // end print last 5 movements of user table
    // last fields in form
    print '<table class="border" width="100%">'."\n";
    //amount
    print '<tr><td class="fieldrequired">'.$langs->trans("Amount").'</td><td><input type="number" name="amount" size="20" step="0.01" min="0.01" value="'.($object->amount?$object->amount:NULL).'"></td></tr>'."\n";
   
    // movement type
    // showed only in case of newdeposit
    if ($action == 'newDeposit') {
        print '<tr><td>'.$langs->trans("MovementType").'</td><td>';
        print $form->selectarray("mov_type", ($action == 'newDeposit'?$object->array_DepositMovement_type:$object->array_PaymentMovement_type), ($object->mov_type>0?$object->mov_type:($action == 'newDeposit'?10:20)));
        print "</td></tr>";
    }
      
    //date
    print '<tr><td class="fieldrequired">'.$langs->trans("Date").'</td>'."\n";
	print '<td>'.$form->selectDate(($object->date_creation ? $object->date_creation : -1), 'date_creation', '', '', 1, 'formsoc').'</td>'."\n";
	print '</td></tr>'."\n";

	// description
    print '<tr><td class="fieldrequired">'.$langs->trans("Description").'</td>'."\n";
    print '<td><textarea rows="4" cols="50" name="description">'.($object->description?$object->description:'').'</textarea></td></tr>'."\n";
    print '</table>'."\n";
   	 
    //hidden info to store
    print '<input type="hidden" name="ref" value="'.$id.'">'."\n";
	
	dol_fiche_end();

	print '<div class="center">';
	// print '<input type="submit" class="button" name="add" value="'.dol_escape_htmltag($langs->trans("Create")).'">';
	print '<input type="submit" class="button" name="add" value="'.dol_escape_htmltag($langs->trans("Create")).'">';
	print '&nbsp; ';
	print '<input type="'.($backtopage?"submit":"button").'" class="button" name="cancel" value="'.dol_escape_htmltag($langs->trans("Cancel")).'"'.($backtopage?'':' onclick="javascript:history.go(-1)"').'>';	// Cancel for create does not post form if we don't know the backtopage
	print '</div>';

	print '</form>';
}

// Part to edit record
/* there is no edition in movements!
   if (($id || $ref) && $action == 'edit')
   {
   print load_fiche_titre($langs->trans("ModAdherents"));

   print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
   print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
   print '<input type="hidden" name="action" value="update">';
   print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
   print '<input type="hidden" name="id" value="'.$object->id.'">';

   dol_fiche_head();

   print '<table class="border centpercent">'."\n";

   // Common attributes
   include DOL_DOCUMENT_ROOT . '/core/tpl/commonfields_edit.tpl.php';

   // Other attributes
   include DOL_DOCUMENT_ROOT . '/core/tpl/extrafields_edit.tpl.php';

   print '</table>';

   dol_fiche_end();

   print '<div class="center"><input type="submit" class="button" name="save" value="'.$langs->trans("Save").'">';
   print ' &nbsp; <input type="submit" class="button" name="cancel" value="'.$langs->trans("Cancel").'">';
   print '</div>';

   print '</form>';
   }
*/
// Part to show record
if ($object->id > 0 && (empty($action) || ($action != 'newDeposit' && $action != 'newPayment')))
{
    $res = $object->fetch_optionals();

    print 'ESTAMOS AQUÍ!!';
	//$head = modadherentsPrepareHead($object);
	//dol_fiche_head($head, 'card', $langs->trans("modadherents"), -1, 'ModAdherents@sociasmercao');

	$formconfirm = '';

	// Confirmation to delete
	if ($action == 'delete')
	{
	    $formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id, $langs->trans('DeleteModAdherents'), $langs->trans('ConfirmDeleteModAdherents'), 'confirm_delete', '', 0, 1);
	}

	// Clone confirmation
	if ($action == 'clone') {
		// Create an array for form
		$formquestion = array();
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id, $langs->trans('CloneModAdherents'), $langs->trans('ConfirmCloneModAdherents', $object->ref), 'confirm_clone', $formquestion, 'yes', 1);
	}

	// Confirmation of action xxxx
	if ($action == 'xxx')
	{
		$formquestion=array();
	    /*
          $forcecombo=0;
          if ($conf->browser->name == 'ie') $forcecombo = 1;	// There is a bug in IE10 that make combo inside popup crazy
          $formquestion = array(
          // 'text' => $langs->trans("ConfirmClone"),
          // array('type' => 'checkbox', 'name' => 'clone_content', 'label' => $langs->trans("CloneMainAttributes"), 'value' => 1),
          // array('type' => 'checkbox', 'name' => 'update_prices', 'label' => $langs->trans("PuttingPricesUpToDate"), 'value' => 1),
          // array('type' => 'other',    'name' => 'idwarehouse',   'label' => $langs->trans("SelectWarehouseForStockDecrease"), 'value' => $formproduct->selectWarehouses(GETPOST('idwarehouse')?GETPOST('idwarehouse'):'ifone', 'idwarehouse', '', 1, 0, 0, '', 0, $forcecombo))
          );
	    */
	    $formconfirm = $form->formconfirm($_SERVER["PHP_SELF"] . '?id=' . $object->id, $langs->trans('XXX'), $text, 'confirm_xxx', $formquestion, 0, 1, 220);
	}

	// Call Hook formConfirm
	$parameters = array('lineid' => $lineid);
	$reshook = $hookmanager->executeHooks('formConfirm', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
	if (empty($reshook)) $formconfirm.=$hookmanager->resPrint;
	elseif ($reshook > 0) $formconfirm=$hookmanager->resPrint;

	// Print form confirm
	print $formconfirm;


	// Object card
	// ------------------------------------------------------------
	$linkback = '<a href="' .dol_buildpath('/sociasmercao/movements_list.php',1) . '?restore_lastsearch_values=1' . (! empty($socid) ? '&socid=' . $socid : '') . '">' . $langs->trans("BackToList") . '</a>';

	$morehtmlref='<div class="refidno">';
	/*
	// Ref bis
	$morehtmlref.=$form->editfieldkey("RefBis", 'ref_client', $object->ref_client, $object, $user->rights->sociasmercao->creer, 'string', '', 0, 1);
	$morehtmlref.=$form->editfieldval("RefBis", 'ref_client', $object->ref_client, $object, $user->rights->sociasmercao->creer, 'string', '', null, null, '', 1);
	// Thirdparty
	$morehtmlref.='<br>'.$langs->trans('ThirdParty') . ' : ' . $soc->getNomUrl(1);
	// Project
	if (! empty($conf->projet->enabled))
	{
    $langs->load("projects");
    $morehtmlref.='<br>'.$langs->trans('Project') . ' ';
    if ($user->rights->sociasmercao->edit)
    {
    if ($action != 'classify')
    $morehtmlref.='<a href="' . $_SERVER['PHP_SELF'] . '?action=classify&amp;id=' . $object->id . '">' . img_edit($langs->transnoentitiesnoconv('SetProject')) . '</a> : ';
    if ($action == 'classify') {
    //$morehtmlref.=$form->form_project($_SERVER['PHP_SELF'] . '?id=' . $object->id, $object->socid, $object->fk_project, 'projectid', 0, 0, 1, 1);
    $morehtmlref.='<form method="post" action="'.$_SERVER['PHP_SELF'].'?id='.$object->id.'">';
    $morehtmlref.='<input type="hidden" name="action" value="classin">';
    $morehtmlref.='<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
    $morehtmlref.=$formproject->select_projects($object->socid, $object->fk_project, 'projectid', 0, 0, 1, 0, 1, 0, 0, '', 1);
    $morehtmlref.='<input type="submit" class="button valignmiddle" value="'.$langs->trans("Modify").'">';
    $morehtmlref.='</form>';
    } else {
    $morehtmlref.=$form->form_project($_SERVER['PHP_SELF'] . '?id=' . $object->id, $object->socid, $object->fk_project, 'none', 0, 0, 0, 1);
    }
    } else {
    if (! empty($object->fk_project)) {
    $proj = new Project($db);
    $proj->fetch($object->fk_project);
    $morehtmlref.=$proj->getNomUrl();
    } else {
    $morehtmlref.='';
    }
    }
	}
	*/
	$morehtmlref.='</div>';


	dol_banner_tab($object, 'ref', $linkback, 1, 'ref', 'ref', $morehtmlref);


	print '<div class="fichecenter">';
	print '<div class="fichehalfleft">';
	print '<div class="underbanner clearboth"></div>';
	print '<table class="border centpercent">'."\n";

	// Common attributes
	//$keyforbreak='fieldkeytoswithonsecondcolumn';
	include DOL_DOCUMENT_ROOT . '/core/tpl/commonfields_view.tpl.php';

	// Other attributes
	include DOL_DOCUMENT_ROOT . '/core/tpl/extrafields_view.tpl.php';

	print '</table>';
	print '</div>';
	print '</div>';

	print '<div class="clearboth"></div><br>';

	dol_fiche_end();


	// Buttons for actions
	if ($action != 'presend' && $action != 'editline') {
    	print '<div class="tabsAction">'."\n";
    	$parameters=array();
    	$reshook=$hookmanager->executeHooks('addMoreActionsButtons',$parameters,$object,$action);    // Note that $action and $object may have been modified by hook
    	if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

    	if (empty($reshook))
    	{
    	    // Send
            print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?id=' . $object->id . '&action=presend&mode=init#formmailbeforetitle">' . $langs->trans('SendMail') . '</a>'."\n";

            // Modify
    		if ($user->rights->sociasmercao->edit)
    		{
    			print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&amp;action=edit">'.$langs->trans("Modify").'</a>'."\n";
    		}
    		else
    		{
    			print '<a class="butActionRefused" href="#" title="'.dol_escape_htmltag($langs->trans("NotEnoughPermissions")).'">'.$langs->trans('Modify').'</a>'."\n";
    		}

    		// Clone
    		if ($user->rights->sociasmercao->edit)
    		{
    			print '<div class="inline-block divButAction"><a class="butAction" href="' . $_SERVER['PHP_SELF'] . '?id=' . $object->id . '&amp;socid=' . $object->socid . '&amp;action=clone&amp;object=order">' . $langs->trans("ToClone") . '</a></div>';
    		}

    		/*
              if ($user->rights->sociasmercao->edit)
              {
              if ($object->status == 1)
              {
              print '<a class="butActionDelete" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&amp;action=disable">'.$langs->trans("Disable").'</a>'."\n";
              }
              else
              {
              print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&amp;action=enable">'.$langs->trans("Enable").'</a>'."\n";
              }
              }
    		*/

    		if ($user->rights->sociasmercao->delete)
    		{
    			print '<a class="butActionDelete" href="'.$_SERVER["PHP_SELF"].'?id='.$object->id.'&amp;action=delete">'.$langs->trans('Delete').'</a>'."\n";
    		}
    		else
    		{
    			print '<a class="butActionRefused" href="#" title="'.dol_escape_htmltag($langs->trans("NotEnoughPermissions")).'">'.$langs->trans('Delete').'</a>'."\n";
    		}
    	}
    	print 'DISTINTO A PRESEND y A EDITLINE!';
    	print '</div>'."\n";
	}


	// Select mail models is same action as presend
	if (GETPOST('modelselected')) {
		$action = 'presend';
	}

	if ($action != 'presend')
	{
	    print '<div class="fichecenter"><div class="fichehalfleft">';
	    print '<a name="builddoc"></a>'; // ancre
        print 'DISTINTO A PRESEND!';
        // Documents
	    /*$objref = dol_sanitizeFileName($object->ref);
          $relativepath = $comref . '/' . $comref . '.pdf';
          $filedir = $conf->sociasmercao->dir_output . '/' . $objref;
          $urlsource = $_SERVER["PHP_SELF"] . "?id=" . $object->id;
          $genallowed = $user->rights->sociasmercao->read;	// If you can read, you can build the PDF to read content
          $delallowed = $user->rights->sociasmercao->create;	// If you can create/edit, you can remove a file on card
          print $formfile->showdocuments('sociasmercao', $objref, $filedir, $urlsource, $genallowed, $delallowed, $object->modelpdf, 1, 0, 0, 28, 0, '', '', '', $soc->default_lang);
		*/

	    // Show links to link elements
	    $linktoelem = $form->showLinkToObjectBlock($object, null, array('modadherents'));
	    $somethingshown = $form->showLinkedObjectBlock($object, $linktoelem);


	    print '</div><div class="fichehalfright"><div class="ficheaddleft">';

	    $MAXEVENT = 10;

	    $morehtmlright = '<a href="'.dol_buildpath('/sociasmercao/modadherents_info.php', 1).'?id='.$object->id.'">';
	    $morehtmlright.= $langs->trans("SeeAll");
	    $morehtmlright.= '</a>';

	    // List of actions on element
	    include_once DOL_DOCUMENT_ROOT . '/core/class/html.formactions.class.php';
	    $formactions = new FormActions($db);
	    $somethingshown = $formactions->showactions($object, 'modadherents', $socid, 1, '', $MAXEVENT, '', $morehtmlright);

	    print '</div></div></div>';
	}

	//Select mail models is same action as presend
	/*
      if (GETPOST('modelselected')) $action = 'presend';

      // Presend form
      $modelmail='inventory';
      $defaulttopic='InformationMessage';
      $diroutput = $conf->product->dir_output.'/inventory';
      $trackid = 'stockinv'.$object->id;

      include DOL_DOCUMENT_ROOT.'/core/tpl/card_presend.tpl.php';
    */
}

// End of page
llxFooter();
$db->close();
